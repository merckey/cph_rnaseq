# RNAseq data analysis lab

Lab on Google Cloud VM:

  * Google Account Login and direct you web browser to [Google Cloud Platform](https://console.cloud.google.com).
  * Access the VM through SSH.
  * Start your VM instance if its current status is "STOP".
  * Change the IP address in the SFTP client to the current VM external IP.

Tools used in this lab:

  1. [FastQC](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
  1. [MapSplice 2](http://www.netlab.uky.edu/p/bioinfo/MapSplice2UserGuide)
  2. [Samtools](https://github.com/samtools/samtools)
  3. [HTSeq](http://www-huber.embl.de/users/anders/HTSeq/doc/overview.html)
  4. [edgeR](http://www.bioconductor.org/packages/release/bioc/html/edgeR.html)

Tools mentioned above were all packed in a Docker container. If you familiar with Virtual Box or VMWare which allows you to run an operating system inside another operating system, docker is similar to them but extremely light weighted. **You need to start the container in the interactive mode for your lab and homework.**

1. Download/update the docker image. It will download the image and will get updated if we add more tools for the lab in the future.
  
        :::bash
        # download docker image
        docker pull merckey/cph738_lab:r_custom
        # check current image available on you VM
        docker images

1. To start the docker container, run the following command. It will start the operating system for the lab.  

        :::bash
        docker run -it  -v ~/cph_rnaseq/:/cph_rnaseq -w /cph_rnaseq  merckey/cph738_lab:r_custom
        # You should be able to run samtools if your are in the container
        samtools
        # Output
        Program: samtools (Tools for alignments in the SAM format)
        Version: 0.1.19-44428cd
        Usage:   samtools <command> [options]
        Command: view        SAM<->BAM conversion
        ......

#### 1. Check your current path
Make sure your are in the folder /cph_rnaseq/. To check your current path, use `pwd`. You can change your directory use `cd`.
```
# Show the current path
pwd
# Output should be:
/cph_rnaseq
# if not change the directory to folder /cph_rnaseq
cd /cph_rnaseq
```

#### 2. Sample dataset for this lab
We used real data from the study [The 8q24 cleft lip susceptibility locus is a remote Myc enhancer required for normal face morphology](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE52974).

We will focus only on the expression estimation of the 4 samples on **Chromosome 19** for this lab.

|Sample ID|Treatment|Data|
|---------|---------|----|
|SRR1042887|Wildtype|SRR1042887.chr19.fastq.gz|
|SRR1042888|Wildtype|SRR1042888.chr19.fastq.gz|
|SRR1042889|del(8-17)|SRR1042889.chr19.fastq.gz|
|SRR1042890|del(8-17)|SRR1042890.chr19.fastq.gz|

##### Check your data
1. Sequencing data

The raw sequencing data are in the folder lab/raw. To show the contents in that folder, use `ls`.
```
ls lab/raw
# output
SRR1042887.chr19.fastq.gz  SRR1042888.chr19.fastq.gz  
SRR1042889.chr19.fastq.gz  SRR1042890.chr19.fastq.gz
```

The suffix of the file names are ".fastq.gz". It means that the fastq files were compressed. We need to uncompress them using `gunzip`.
```
gunzip lab/raw/*.gz
# run ls lab/raw again to show the changes
SRR1042887.chr19.fastq  SRR1042888.chr19.fastq
SRR1042889.chr19.fastq  SRR1042890.chr19.fastq
```
You can check the file content using `head`. The following command will print the first 12 lines in a fastq file.
```
head -12 lab/raw/SRR1042887.chr19.fastq
```

2. Reference data

The reference data are in lab/ref. 2 files are included.
```
# Mus_musculus GRCm38 reference genome
19.fa.gz
# Mus_musculus GRCm38 gene annotation file
Mus_musculus.GRCm38.86.chr19.gtf.gz
```

#### 3. Quality control
When you get the data, the first thing you need to do is check the data quality.
We use [FastQC](http://www.bioinformatics.babraham.ac.uk/projects/fastqc/) to check the quality of the sequence data.
The following script `scripts/1_fastqc.sh` will run FastQC on every fastq files located in `lab/raw`. The `-d` parameter allow you to run 
fastqc for the data located in ***lab*** or ***hw*** folder.

```
bash scripts/1_fastqc.sh -d lab
# output
Started analysis of SRR1042887.chr19.fastq
Approx 5% complete for SRR1042887.chr19.fastq
Approx 10% complete for SRR1042887.chr19.fastq
Approx 15% complete for SRR1042887.chr19.fastq
Approx 20% complete for SRR1042887.chr19.fastq
.........
```

#### 4. Align Reads and Feature Counting

We will use `scripts/2_align_count.sh` script to estimate the gene count using the fastq files.






  1. Align the sequences for each sample to the reference genome using [MapSplice 2](http://www.netlab.uky.edu/p/bioinfo/MapSplice2UserGuide) which output a [SAM](https://samtools.github.io/hts-specs/SAMv1.pdf) file (Sequence Alignment/Map format). 
  1. Convert the SAM files to BAM which is compressed SAM file allowing efficient random access for indexed queries. We can also visualize the alignment using the sorted BAM file with a tool called [IGV](https://software.broadinstitute.org/software/igv/download).
  1. Assign the aligned reads to the gene features in the GTF annotation data using `HTSeq`.

**How mapsplice works:**

![mapsplice](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2952873/bin/gkq622f2.jpg)

**How htseq works:**

![htseqcount](https://htseq.readthedocs.io/en/release_0.11.1/_images/count_modes.png)

```
# Usage:
# bash scripts/2_align_count.sh -s SAMPLE_ID -d DIRECTORY
# For example, if you want to process SRR1042887 in the lab folder,
# you can type in the following command

bash scripts/2_align_count.sh -s SRR1042887 -d lab

# When finishes, check the folder lab/results/mapsplice2/SRR1042887/
ls lab/results/mapsplice2/SRR1042887/

# For homework, replace "lab" with "hw"
bash scripts/2_align_count.sh -s SRR1042887 -d hw

```

#### 5. Merge the count data to table
After you process all the samples, we need to combine them into a single table for the differential expression test in the next step. The R script *merge_htseq.R* will generate the table from you.

```
# merge htseq results
Rscript scripts/merge_htseq.R \
--rootDir=lab/results/mapsplice2 --pattern=htseq.tsv \
--outFile=lab/results/gene.count.tsv
```

#### 6. Differential Expression analysis using edgeR
You can now download the merged htseq count data to you labtop and run the differential expression analysis based on `scripts/edgeR.R` in RStudio interactively. Or you can open an R session on your instance using R Console.


If you don't have edgeR library installed, open a R session and run:
```
source("https://bioconductor.org/biocLite.R")
biocLite("edgeR")
```
