#!/bin/bash

sudo apt-get update
sudo apt-get install -y git python-pip python-dev

sudo docker ps
if [ "$?" != "0" ]; then
  sudo apt-get install -y docker.io
  sudo usermod -aG docker $USER
fi

exit
