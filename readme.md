# CPH738 Lab

## 1. Setup Google Cloud Platform
[Setup Google Cloud](https://bitbucket.org/merckey/cph738_lab/src/70b44d650a0379cf60a0284024ea8912eb36d62f/google_cloud.MD)

[Setup SFTP](https://bitbucket.org/merckey/cph738_lab/src/master/setup_sftp.md)


## 2. Initialize the VM
After connect to your VM, run the following scripts one by one to initialized the VM.
It will download the data for the lab and homework.

```
sudo apt-get update
sudo apt-get install -y git
git clone https://merckey@bitbucket.org/merckey/cph_rnaseq.git
cd cph_rnaseq
bash install_ubuntu.sh
# close the connection
exit
```

## 3. Install docker
The lab and homework need several tools for data analysis. We use docker to distribute the same environment to you so that everyone can get exact the same setup.

Run the following command to download.
```
# Download the docker container
docker pull merckey/cph738_lab:r_custom
```

After the download, you can check the success by run the follow command.
```
# List all the docker images available
docker images
# You should see similar output as below:
# REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
# merckey/cph738_lab   r_custom            b9ab309c6e71        19 hours ago        1.064 GB
```

To get access to the tools and data in the docker container, you need to start the container in the interactive mode:
```
docker run -it  -v ~/cph_rnaseq/:/cph_rnaseq -w /cph_rnaseq  merckey/cph738_lab:r_custom
```
