#!/bin/bash
set -e
# Perform quality control using FastQC

usage() {
  cat << EOF
  usage: $0 options
  OPTIONS:
  -h      Show help message
  -d      Data directory, either 'lab' or 'hw'
EOF
}

while getopts "hd:" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
    ;;
    d)
      DATA_DIRECTORY=$OPTARG
    ;;
    \?)
      usage
      exit
    ;;
  esac
done

###############
# setup input #
###############
ROOT="/cph_rnaseq/$DATA_DIRECTORY"
FASTQ_DIR=$ROOT"/raw/"

echo $READs
# ################
# # setup output #
# ################
if [ -z "$DATA_DIRECTORY" ]
then
   usage
   exit 1
fi

FASTQC_DIR=$ROOT"/results/fastqc/"

if [[ -n $(find $FASTQ_DIR -name "*.gz") ]]
then
    gunzip $FASTQ_DIR/*.gz
fi
# create output folder for fastqc
mkdir -p $FASTQC_DIR
# run fastqc
fastqc $FASTQ_DIR/*.fastq -o $FASTQC_DIR
