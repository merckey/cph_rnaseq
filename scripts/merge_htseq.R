# MERGE htseq gene expression output into a single count file
# Author: Jinpeng Liu

suppressPackageStartupMessages(library("optparse"))
optList <- list(
  make_option("--rootDir", default = '.', help = "Root directory to search [default %default]"),
  make_option("--pattern", default = 'genes.results', help = "Expression file name pattern [default %default]"),
  make_option("--outFile", default = "gene.count.tsv", help = "Output results to this file [default %default]"),
  make_option("--annotFile", default = NULL, help = "Pre-download annotation rdata from biomaRt [default %default]"),
  make_option("--rmNoSymbol", default = TRUE, help = "Remove genes with no hgnc_symbol [default %default]")
  )

parser <- OptionParser(usage = "%prog [options]", option_list = optList)

arguments <- parse_args(parser, positional_arguments = T)

opt <- arguments$options

rootDir <- opt$rootDir
pattern <- opt$pattern
outFile <- opt$outFile


files <- list.files(path = rootDir, pattern = pattern, recursive = TRUE, full.names = TRUE)
nfiles <- length(files)

if(nfiles==0){
  cat("No expression data found in the defined target folder\n")
  print_help(parser)
  stop()
}else{
  cat(paste("Found", nfiles, "files\n"))
}

myCol <- c("character","numeric")
for (file in files){
  cat(paste("Merging", file, "\n"))
  print(sub("\\..*",'',basename(file)))
  if (!exists("dataset")){
    dataset <- read.table(file, header=FALSE, colClasses = myCol,
                          col.names = c("gene_id", sub("\\..*",'',basename(file))), sep="\t",check.names=FALSE)
    print(dim(dataset))
    #write.table(dataset,sub("\\..*",'',basename(file)), row.names=F,quote=F, sep='\t')
  }else{
    temp_dataset <- read.table(file, header=TRUE, colClasses = myCol,
                        col.names = c("gene_id", sub("\\..*",'',basename(file))), sep="\t",check.names=FALSE)
    #write.table(temp_dataset,sub("\\..*",'',basename(file)), row.names=F,quote=F, sep='\t')
    dataset<-merge(dataset, temp_dataset, by = "gene_id")
    rm(temp_dataset)
  }
}
if((nfiles+1) != ncol(dataset)){
  cat(paste("Merged table has", ncol(dataset), "but found", nfiles,'\n'))
  stop()
}

write.table(dataset, outFile, row.names=F,quote=F, sep='\t')

if(!is.null(opt$annotFile)){
  load(opt$annotFile)
  warning(paste("Annotation file contains",nrow(ensembl_df), "entries"))
  idx = match(dataset$gene_id,ensembl_df$ensembl_gene_id)
  warning(paste(length(which(!is.na(idx))), "genes are in the annotation file"))
  dataset = cbind(dataset[!is.na(idx),], ensembl_df[idx[!is.na(idx)],])
  if(opt$rmNoSymbol){
    warning("Removing ensembl_gene with no hgnc_symbol!")
    dataset = dataset[dataset$hgnc_symbol != '',]
  }
  outFile = sub("tsv$","annotated.tsv",outFile)
  write.table(dataset, outFile, row.names=F,quote=F, sep='\t')

}
