#!/bin/bash
set -e
# Perform read alignment using MapSplice2
# Usage: bash 2_alignment.sh -s SAMPLE_ID -d DIRECTORY
usage() {
  cat << EOF
  usage: $0 options
  This script align single-end RNASeq reads to reference genome using MapSplice2 and count reads for each gene
  OPTIONS:
  -h      Show help message
  -s      Sample_id, can be SRR1042887, SRR1042888, SRR1042889 or SRR1042890
  -d      Data directory, either 'lab' or 'hw'
EOF
}

while getopts "hs:d:" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
    ;;
    s)
      SAMPLE_ID=$OPTARG
    ;;
    d)
      DATA_DIRECTORY=$OPTARG
    ;;
    \?)
      usage
      exit
    ;;
  esac
done

###########################################################################
# path for MapSplice2
MAPSPLICE_BIN="/opt/MapSplice-v2.2.1/mapsplice.py"
# path for inputs
ROOT="/cph_rnaseq/$DATA_DIRECTORY/"
REF_GENOME=$ROOT"ref/"
REF_ANNOT=$ROOT"ref/"*.gtf
FASTQ=$(echo $ROOT"/raw/"$SAMPLE_ID.*.fastq*)

RESULTS_DIR=$ROOT"results/mapsplice2/"$SAMPLE_ID
INDEX=$ROOT"ref/index/bw"

# Numbers of CPUs to use
THREADS=4

if [ ! -e $FASTQ ]; then
  echo "fastq file for sample $SAMPLE_ID doesn't exist!"
  usage
    exit 1
fi

# make sure unzip
if [[ ${FASTQ##*.} = gz ]]; then
  echo $FASTQ
  gunzip $FASTQ
  FASTQ=$(echo $ROOT"/raw/"$SAMPLE_ID.*.fastq)
fi

if [[ -n $(find $REF_GENOME -name "*.gz") ]]
then
    gunzip $REF_GENOME/*.gz
fi
# Create necessary folders
mkdir -p $(dirname $INDEX) $RESULTS_DIR

# run MapSplice2
echo "Run MapSplice2 read alignment ..."
python $MAPSPLICE_BIN \
-o $RESULTS_DIR/ \
-c $REF_GENOME \
-x $INDEX \
-p $THREADS \
-1 $FASTQ


# convert sam to bam, sort and index bam file
echo "Sam to bam conversion ... "

samtools view -Sbh $RESULTS_DIR/alignments.sam > $RESULTS_DIR/alignments.bam

samtools sort $RESULTS_DIR/alignments.bam  $RESULTS_DIR/alignments.sorted

samtools index $RESULTS_DIR/alignments.sorted.bam

# run HTSeq to count features
echo "Run HTSeq feature couting ... "
htseq-count -m intersection-nonempty -i gene_id \
-s no \
-r name \
$RESULTS_DIR/alignments.sam \
$REF_ANNOT > $RESULTS_DIR/$SAMPLE_ID.htseq.tsv

sed -i '/^_/d' $RESULTS_DIR/$SAMPLE_ID.htseq.tsv
